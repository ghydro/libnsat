/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: manage_zns.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_zns.c
 * @brief       Functions dealing with the attributes of the s_zns structure.
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#if defined(COUPLED)
#include "NSAT.h"
#else
#include "NSAT_full.h"
#endif

/**
 * @brief Allocates and initializes attributes of a Nash reservoir cascade.
 * @return All-set reservoir cascade.
 */
s_zns *NSAT_create_zns(int code, s_nash *pnash) {

  s_zns *pzns;

  pzns = new_zns();
  bzero((char *)pzns, sizeof(s_zns));
  pzns->id[NS_GIS] = code;
  pzns->pnash = pnash;
  pzns->nres = (int *)calloc(NS_TSTEP, sizeof(int));
  pzns->nres[PREVTS_NS] = pnash->nb_reservoir;
  pzns->nres[CURRTS_NS] = pnash->nb_reservoir;
  pzns->pcatchment_chasm = NULL;        // NG : 23/01/2020
  pzns->wat_bal = RSV_create_wat_bal(); // NF : 24/10/2004
  pzns->dyn_io = NO;                    // NG : 23/06/2023
  pzns->soil_elevation = CODE_NSAT;
  pzns->hydhead = NULL; // NG : 27/06/2023 : Table of libaq hydraulic heads at previous and current NSAT update time
  pzns->Vr = 0.;
  pzns->Fr = NULL;
  pzns->trvar_init = NULL;
  return pzns;
}

/**
 * @brief Links two NSAT cells
 */
s_zns *NSAT_secured_chain_zns_fwd(s_zns *pzns1, s_zns *pzns2) {
  if (pzns2 != NULL) {
    pzns1 = NSAT_chain_zns_fwd(pzns1, pzns2);
  } else {
    pzns1 = pzns2;
  }
  return pzns1;
}

/**
 * @brief Links two NSAT cells
 */
s_zns *NSAT_chain_zns_fwd(s_zns *pzns1, s_zns *pzns2) {

  pzns1->next = pzns2;
  pzns2->prev = pzns1;
  return (pzns1);
}

/**
 * @brief Checks if links between chasm-type catchments and nsat units are correct : functions actually checks if sum of the
   infiltration coefficients per catchement redirected to the unsaturared zone is equal to 100% in order to avoid fluxes loss.
 */
void NSAT_check_chasm_connexion(s_carac_zns *pcarac_zns, FILE *fpout) { // NG : 27/03/2020

  s_zns *pzns;
  int nb_zns = pcarac_zns->nb_zns;
  int i, curr;
  int max_id_catch = SMALL_NSAT;
  double *tab_coef;

  // Fetch of the max GIS id of linked catchments
  for (i = 0; i < nb_zns; i++) {
    pzns = pcarac_zns->p_zns[i];
    if (pzns->pcatchment_chasm != NULL) {
      if (pzns->pcatchment_chasm->id_gis > max_id_catch)
        max_id_catch = pzns->pcatchment_chasm->id_gis;
    }
  }

  tab_coef = (double *)malloc(max_id_catch * sizeof(double));
  if (tab_coef == NULL)
    LP_error(fpout, "Bad memory allocation in function %s, in file %s at line %d.\n", __func__, __FILE__, __LINE__);
  for (i = 0; i < max_id_catch; i++)
    tab_coef[i] = SMALL_NSAT;

  // Calculating the sum of infiltration coefficients for each catchment
  for (i = 0; i < nb_zns; i++) {
    pzns = pcarac_zns->p_zns[i];
    if (pzns->pcatchment_chasm != NULL) {
      curr = pzns->pcatchment_chasm->id_gis - 1;
      if (tab_coef[curr] < 0)
        tab_coef[curr] = 0;
      tab_coef[curr] += pzns->pcatchment_chasm->infil_coef;
    }
  }

  for (i = 0; i < max_id_catch; i++) {
    if (tab_coef[i] > 0 && fabs(tab_coef[i] - 1.0) > EPS_NSAT) {
      LP_warning(fpout, "%11.7f prct of chasm flux towards NSAT zone will be lost for catchment GIS ID %d !\n", fabs(tab_coef[i] - 1.0) * 1.e2, i + 1);
    }
  }
  free(tab_coef);
}

/**
 * @brief Water flow calculation in a Nash reservoir cascade (non-layered reservoirs)
 * @internal NG : 28/06/2023 : Function update with inclusion of dynamic thickness option.
 */
void NSAT_calculate_zns(s_zns *pzns, double dt, int dynflag, FILE *fpout) {

  int j, i;
  double z_init;
  s_reservoir_rsv *prsv;
  s_wat_bal_rsv *pwat_bal;

  j = 0;

  pwat_bal = pzns->wat_bal;
  if (pzns->prsv != NULL) {

    // Update of the reservoir chain structure if dynamic thickness is activated
    if (dynflag == YES)
      NSAT_dynamic_chain_update(pzns, fpout);

    prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, pzns->prsv, BEGINNING_SPA);
    z_init = prsv->z;
    prsv->z += pwat_bal->q[RSV_INFILT]; // Adding infiltration rate to the chain's top reservoir
    RSV_reinit_wat_bal(pwat_bal);
    pwat_bal->q[RSV_INFILT] = (prsv->z - z_init); // Assinging ZNS input water to ZNS watbal (in mm)

    while (prsv != NULL) {

      if (j > 0) {
        z_init = prsv->z;
      }
#ifdef DEBUG
      LP_printf(fpout, "--> z_init %f\n", z_init); // BL to debug
#endif
      j++;
      RSV_transit_simple(pwat_bal, pzns->pnash->carac_ZNS[NS_HEIGHT], pzns->pnash->carac_ZNS[NS_C_QOUT], pzns->pnash->carac_ZNS[NS_BOND_WATER], RSV_SOUT, prsv, NO, RSV_SOUT, fpout);
#ifdef DEBUG
      LP_printf(fpout, "Reservoir %i outflow: %lf\n", j, pwat_bal->q[RSV_SOUT]); // BL to debug
#endif
      pwat_bal->q[RSV_BIL] += prsv->z - z_init; // ZNS storage
      prsv = prsv->next;
    }
  } else {
    // LP_warning(fpout,"no reservoir for zns ID : %d",pzns->id[NS_INTERN]);
    pwat_bal->q[RSV_SOUT] = pwat_bal->q[RSV_INFILT]; // i.e. IN=OUT
  }

  if (dynflag == YES)
    pwat_bal->q[RSV_SOUT] += pzns->Vr; // Dropping total water amount of erased reservoirs to subsurface aquifer

  // Calculating water budget in m3/s.
  for (i = 0; i < RSV_NTYPE; i++) {
    pwat_bal->q[i] = RSV_mm_to_q(pwat_bal->q[i], pzns->area, dt, DAY_TS, fpout);
  }
}

/**
 * @brief In dynamic thickness mode : Adds (and initializes) or destroys reservoirs chains
 * according to hydraulic head variations. pzns->Vr is the water height (mm) to be added
 * to the aquifer recharge.
 * In case of a hydraulic head decrease, a new reservoir may be added to the bottom of the nsat
 * cell, initialize to z=bond_water. If increase, 1 (or more) reservoir water content is
 * flushed into the aquifer.
 */
void NSAT_dynamic_chain_update(s_zns *pzns, FILE *fpout) {

  s_reservoir_rsv *prsv, *new_prsv;
  int i, j, nres_delta = 0;
  double *caracZNS;

  // Reinit
  pzns->Vr = 0.;
  nres_delta = pzns->nres[CURRTS_NS] - pzns->nres[PREVTS_NS];

  if (pzns->dyn_io == YES && (nres_delta != 0)) {
    prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, pzns->prsv, END_SPA);

    if (nres_delta > 0) { // Hydraulic head decrease

      // Adding and initializing new reservoirs with pzns nash parameters
      for (i = 0; i < nres_delta; i++) {
        caracZNS = pzns->pnash->carac_ZNS;
        new_prsv = RSV_create_reservoir(caracZNS[NS_HEIGHT], caracZNS[NS_C_QOUT], caracZNS[NS_BOND_WATER]);
        new_prsv->z = new_prsv->bond_water; // 24/11/2024 Fix : init was missing
        prsv = RSV_secured_chain_rsv_fwd(prsv, new_prsv);
      }
    } else { // Hydhead increase
      j = 0;
      while (j < nres_delta) {
        pzns->Vr += prsv->z;
        j++;
        prsv = prsv->prev;
        RSV_free_rsv(prsv->next);
      }
    }
  }
}

/**
 * @brief Computes, at a given time step, coupled water and transport flows through a column of layered reservoirs.
 * Computations associated with a single water input at the top of the column and several transport inputs (one for each species).
 * @warning RSV_transit_layered_reservoir() operates on multiple species (i.e. SOLUTE) at once.
 * Therefore, not suited to mix HEAT and SOLUTE in the same simulation.
 * For now, LP_error is displayed when trying to use HEAT transport if reservoir column is defined (nres > 0).
 * Not the best move, but safer to avoid user errors.
 */
void NSAT_calculate_zns_transport(int nb_species, int *spetypes, s_zns *pzns, int dynFlag, double dt, FILE *fpout) {

  s_wat_bal_rsv *pwatbal = pzns->wat_bal;
  s_reservoir_rsv *prsv, *tmp;
  int p, i, j, answer;

  // Initializations
  pwatbal->q[RSV_SOUT] = pwatbal->q[RSV_INFILT];
  pwatbal->q[RSV_BIL] = 0.;

  for (p = 0; p < nb_species; p++) {
    pwatbal->trflux[p][RSV_SOUT] = pwatbal->trflux[p][RSV_INFILT];
    pwatbal->trflux[p][RSV_BIL] = 0.;
    pwatbal->trvar[p][RSV_SOUT] = pwatbal->trvar[p][RSV_INFILT];
    pwatbal->trvar[p][RSV_BIL] = 0.;
  }

  // Flows calculation in column if defined
  if (pzns->prsv != NULL) {

    answer = NSAT_is_heat_transport(nb_species, spetypes);
    if (answer == YES) {
      LP_error(fpout, "In libnsat%4.2f : Function %s, in file %s at line %d : Heat transport using Nash reservoir cascade not allowed.\n", VERSION_NSAT, __func__, __FILE__, __LINE__);
    }

    // Update of the reservoir chain structure if dynamic thickness is activated
    if (dynFlag == YES) {
      NSAT_dynamic_chain_update_transport(pzns, nb_species, fpout);
    }

    j = 0;
    prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, pzns->prsv, BEGINNING_SPA);
    while (prsv != NULL) {
      j++;
      RSV_transit_layered_reservoir(j, pzns->id[NS_GIS], nb_species, prsv, pwatbal, RSV_SOUT, fpout); // NG : j and id useful for debugging purposes.
      prsv = prsv->next;
    }
  }

  if (dynFlag == YES) {
    pwatbal->q[RSV_SOUT] += pzns->Vr; // Adding water of erased reservoirs (mm) for update of the aquifer recharge
    for (p = 0; p < nb_species; p++) {
      pwatbal->trflux[p][RSV_SOUT] += pzns->Fr[p]; // Adding flux of erased reservoirs (g/m2) for updates of the aquifer recharge concentration

      if (pwatbal->q[RSV_SOUT] > EPS_RSV) {
        for (p = 0; p < nb_species; p++)
          pwatbal->trvar[p][RSV_SOUT] = pwatbal->trflux[p][RSV_SOUT] / (pwatbal->q[RSV_SOUT] * 1e-3); // g/m3
      }
    }
  }

  // Reservoir water budget conversion from mm to m3/s
  for (i = 0; i < RSV_NTYPE; i++)
    pwatbal->q[i] = RSV_mm_to_q(pwatbal->q[i], pzns->area, dt, DAY_TS, fpout);
}

/**
 * @brief In dynamic thickness mode (Transport mode): Adds (and initializes) or destroys reservoirs chains
 * according to hydraulic head variations.
 * In case of a hydraulic head decrease, a new reservoir may be added to the bottom of the nsat
 * cell, initialized to z=bond_water and the aquifer concentration. If increase, 1 (or more) reservoir
 * water and matter content is flushed into the aquifer.
 */
void NSAT_dynamic_chain_update_transport(s_zns *pzns, int nb_species, FILE *fpout) {

  s_reservoir_rsv *prsv, *new_prsv;
  int i, j, l, p, nres_delta = 0;
  double *caracZNS;

  // Reinit
  pzns->Vr = 0.;
  for (p = 0; p < nb_species; p++)
    pzns->Fr[p] = 0.;

  nres_delta = pzns->nres[CURRTS_NS] - pzns->nres[PREVTS_NS];

  if (pzns->dyn_io == YES && (nres_delta != 0)) {
    prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, pzns->prsv, END_SPA);

    if (nres_delta > 0) { // Hydraulic head decrease

      for (i = 0; i < nres_delta; i++) { // Adding, initializing new reservoirs and associated layers chunk
        caracZNS = pzns->pnash->carac_ZNS;
        new_prsv = RSV_create_reservoir(caracZNS[NS_HEIGHT], caracZNS[NS_C_QOUT], caracZNS[NS_BOND_WATER]);
        new_prsv->z = new_prsv->bond_water;                                             // 24/11/2024 Fix : init was missing
        RSV_initialize_layers_transport(new_prsv, nb_species, pzns->trvar_init, fpout); // trvar_init is the aquifer concentration set to initialize new layers
        prsv = RSV_secured_chain_rsv_fwd(prsv, new_prsv);
      }
    } else { // Hydhead increase
      j = 0;
      while (j < nres_delta) { // Fetching entire water and flux amount to drop in the aquifer

        j++;

        // Water
        for (l = 0; l < prsv->nb_layer; l++) // Reminder : nb_layer is the number of effective layers containing water and flux (not the total BUFFER size !)
          pzns->Vr += prsv->p_layer[l]->height;

        // Flux
        for (l = 0; l < prsv->nb_layer; l++) {
          for (p = 0; p < nb_species; p++) {
            pzns->Fr[p] += (prsv->p_layer[l]->transp_var[p] * prsv->p_layer[l]->height * 1e-3); // g/m2
          }
        }
        prsv = prsv->prev;
        RSV_free_rsv(prsv->next);
      }
    }
  }
}

/**
 * @brief Creates and initializes a Nash cascade (hydro mode)
 */
void NSAT_create_cascade(s_zns *pzns, FILE *fpout) {

  s_reservoir_rsv *prsv2, *prsv;
  int nb_res, indice = 0;
  s_nash *pnash;

  pnash = pzns->pnash;
  prsv = prsv2 = NULL;

  if (pzns->nres[CURRTS_NS] > 0) {

    prsv = RSV_create_reservoir(pnash->carac_ZNS[NS_HEIGHT], pnash->carac_ZNS[NS_C_QOUT], pnash->carac_ZNS[NS_BOND_WATER]);
    prsv->z = pnash->carac_ZNS[NS_RINI];
    nb_res = pzns->nres[CURRTS_NS] - 1; // NF 29/10/04

    for (indice = 0; indice < nb_res; indice++) {
      prsv2 = RSV_create_reservoir(pnash->carac_ZNS[NS_HEIGHT], pnash->carac_ZNS[NS_C_QOUT], pnash->carac_ZNS[NS_BOND_WATER]);
      prsv2->z = pnash->carac_ZNS[NS_RINI];

      if (prsv != NULL) {
        RSV_secured_chain_rsv_fwd(prsv, prsv2);
        prsv = prsv->next;
      } else {
        prsv = prsv2;
      }
    }
    pzns->prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, prsv, BEGINNING_SPA); // NF 29/10/04
  } else {
    pzns->prsv = prsv;
  }
}

/**
 * @brief TBD
 * @return TBD
 */
s_zns *NSAT_check_zns(s_zns *one_zns, s_zns *zns_list, int link_type, FILE *fpout) {
  s_zns *zns_tmp;
  int id_zns, check_id = CODE_TS;
  s_id_spa *link;

  id_zns = one_zns->id[NS_GIS];
  if (id_zns == zns_list->id[NS_GIS]) {
    zns_tmp = NULL;
    zns_list->link[link_type] = SPA_secured_chain_id_fwd(zns_list->link[link_type], one_zns->link[link_type]);
    zns_list->area += one_zns->link[link_type]->prct;
  } else {

    zns_list = (s_zns *)SPA_browse_all_struct(NSAT_SPA, zns_list, END_SPA);

    if (zns_list == NULL) {
      LP_error(fpout, "In libnsat%4.2f : Function %s, in file %s at line %d : Null pointer found.\n", VERSION_NSAT, __func__, __FILE__, __LINE__);
    }
    while (zns_list->prev != NULL || check_id == id_zns) {
      check_id = zns_list->id[NS_GIS];
      zns_list = zns_list->prev;
    }
    if (zns_list->prev != NULL) {
      zns_tmp = NULL;
      zns_list->link[link_type] = SPA_secured_chain_id_fwd(zns_list->link[link_type], one_zns->link[link_type]);
      zns_list->area += one_zns->link[link_type]->prct;
      zns_list = (s_zns *)SPA_browse_all_struct(NSAT_SPA, zns_list, BEGINNING_FP);
    } else {
      zns_tmp = one_zns;
    }
  }
  return zns_tmp;
}

/**
 * @brief Stores all s_zns* into a single carac array
 * @return Allocated and filled s_zns** array
 */
s_zns **NSAT_tab_zns(s_zns *pzns, int nb_zns, FILE *fpout) {
  int i = 0;
  s_zns **tab_zns;

  tab_zns = (s_zns **)malloc(nb_zns * sizeof(s_zns *));
  bzero((char *)tab_zns, nb_zns * sizeof(s_zns *));
  pzns = (s_zns *)SPA_browse_all_struct(NSAT_SPA, pzns, BEGINNING_SPA);

  for (i = 0; i < nb_zns; i++) {
    pzns->id[NS_INTERN] = i;
    tab_zns[i] = pzns;
    pzns = pzns->next;
  }
  return tab_zns;
}

/**
 * @brief Calculates the infiltration water discharge input from libfp link
 * for a single zns cell
 */
void NSAT_define_input(s_zns *pzns, s_carac_fp *pcarac_fp, FILE *fpout) {

  double *output_fp;
  int id_bu, i;
  s_bal_unit_fp *pbu;
  s_id_spa *link;

  link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[SURF_NS], BEGINNING_SPA);
  pzns->wat_bal->q[RSV_INFILT] = 0;
  while (link != NULL) {
    id_bu = link->id;
    pbu = pcarac_fp->p_bu[id_bu];
    output_fp = SPA_calcul_values(CPROD_SPA, pbu, link->prct, 0.0, 0.0, fpout);
    pzns->wat_bal->q[RSV_INFILT] += output_fp[RSV_INFILT];
    link = link->next;
    free(output_fp);
  }
}

/**
 * @brief Computes infiltrated transport values (flux (energy/matter) and variable (concentration/temperature) inputs on
 * top of the ZNS column for a single column at each time step, and for all species.
 */
void NSAT_define_transport_input(int nb_species, int *spe_types, s_zns *pzns, s_carac_fp *pcarac_fp, double dt, FILE *fpout) {

  s_id_spa *link;
  int id_bu;
  s_bal_unit_fp *pbu;
  double *output_flux;
  double vol_water = 0.;
  int p;

  if (pzns->wat_bal->trvar == NULL || pzns->wat_bal->trflux == NULL) {
    LP_error(fpout, "In libnsat%4.2f : Error in file %s, function %s at line %d : Transport-related watbal pointer is NULL for ZNS cell GIS id %d.\n", VERSION_NSAT, __FILE__, __func__, __LINE__, pzns->id[NS_GIS]);
  }

  for (p = 0; p < nb_species; p++) {
    // LP_printf(fpout, "In NSAT zone : in function %s : Specie type for specie ID %d : %s.\n",__func__,p,NSAT_species_type(spe_types[p])); // NG check
    RSV_reinit_matter_fluxes_bal(pzns->wat_bal, p);

    link = (s_id_spa *)SPA_browse_all_struct(IDS_SPA, pzns->link[SURF_NS], BEGINNING_SPA);

    // Input for transport flux (energy (W/m2) or matter (g/m2))
    while (link != NULL) {
      id_bu = link->id;
      pbu = pcarac_fp->p_bu[id_bu];
      output_flux = SPA_calcul_transport_fluxes(CPROD_SPA, pbu, p, link->prct, 0., 0., fpout);
      pzns->wat_bal->trflux[p][RSV_INFILT] += output_flux[FP_MINF];
      link = link->next;
      free(output_flux);
    }

    // Input for water input (mm --> m3)
    vol_water = RSV_mm_to_volume(pzns->wat_bal->q[RSV_INFILT], pzns->area, dt, SEC_TS, fpout);

    // Calculation of transport variable (concentration/temperature)
    switch (spe_types[p]) {
    case HEAT_NS: {
      if (vol_water > EPS_RSV) {
        // Temperature in K
        pzns->wat_bal->trvar[p][RSV_INFILT] = (pzns->wat_bal->trflux[p][RSV_INFILT] * pzns->area) / ((vol_water / (dt * TS_unit2sec(DAY_TS, fpout)) * RHO_WATER_NS * CP_WATER_NS));
      }
      break;
    }
    case SOLUTE_NS: {
      if (vol_water > EPS_RSV) {
        // Concentration in g/m3
        pzns->wat_bal->trvar[p][RSV_INFILT] = (pzns->wat_bal->trflux[p][RSV_INFILT] * pzns->area) / vol_water;
      }
      break;
    }
    default:
      LP_error(fpout, " In function %s : Unknown specie type.\n", __func__);
    }

    // LP_printf(fpout, "ENTERING ZNS : Species %d Type %s ZNS GIS ID %d : Linf [mm] %f Finf [g/m2] %f Cinf [g/m3] %f.\n",
    //            p, NSAT_species_type(spe_types[p]), pzns->id[NS_GIS], (vol_water / pzns->area) * 1000., pzns->wat_bal->trflux[p][RSV_INFILT], pzns->wat_bal->trvar[p][RSV_INFILT]);  // NG check
  }
}

/**
 * @brief Update of the NSAT column water input if connected to a chasm catchment
 * Water input already calculated (infiltration of river discharge or run-off from CPROD)
 * Q_chasm is in m3/s when entering the function.
 */
void NSAT_refresh_input(s_zns *pzns, FILE *fpout, double dt, double t) {

  double Qadd = 0.;

  Qadd = RSV_q_to_mm(pzns->pcatchment_chasm->Q_chasm, pzns->area, dt, DAY_TS, fpout);
  pzns->wat_bal->q[RSV_INFILT] += Qadd;

  // LP_printf(fpout,"Chasm_IDGIS %d t %lf Qaddm3s %18.10lf\n",pzns->pcatchment_chasm->pcprod_outlet->id[FP_GIS],t,pzns->pcatchment_chasm->Q_chasm);  // NG check
  // LP_printf(fpout,"Chasm_IDGIS %d t %lf Sum %18.10lf\n",pzns->pcatchment_chasm->pcprod_outlet->id[FP_GIS],t,pzns->wat_bal->q[RSV_INFILT]);
}

/**
 * @brief Updates input transport fluxes values at the top of the ZNS column if connected to a
 *         run-off chasm type cell-prod. Updates transport variables values
 * @warning Case of river-infiltration inputs induced not treated so far. Run-off chasm type only.
 */
void NSAT_refresh_input_transport(int nb_species, int *spetypes, s_zns *pzns, FILE *fpout, double dt, double t) {
  int p;
  double Fcatch, vol = 0.;

  // Fetching already-updated water volume in m3 if chasm-connexion detected
  vol = RSV_mm_to_volume(pzns->wat_bal->q[RSV_INFILT], pzns->area, dt, SEC_TS, fpout);

  for (p = 0; p < nb_species; p++) {

    switch (spetypes[p]) {
    case HEAT_NS: {
      LP_warning(fpout,
                 "In libnsat%4.2f : File %s, function %s : Specie %d, Type : %s : Update of NSAT inputs due to chasm-connexion from "
                 "surface not implemented yet. ENERGY FLUX LOSS !",
                 VERSION_NSAT, __FILE__, __func__, p, NSAT_species_type(spetypes[p]));
      break;
    }
    case SOLUTE_NS: {
      // Chasm-induced flux matter (in g/m2)
      Fcatch = pzns->pcatchment_chasm->pcprod_outlet->pwat_bal->trflux[p][RSV_RUIS] * pzns->pcatchment_chasm->infil_coef;
      pzns->wat_bal->trflux[p][RSV_INFILT] += Fcatch;
      // Concentration update (in g/m3)
      if (vol > EPS_RSV) {
        pzns->wat_bal->trvar[p][RSV_INFILT] = (pzns->wat_bal->trflux[p][RSV_INFILT] * pzns->area) / vol;
      }
      break;
    }
    default:
      LP_error(fpout, " In function %s : Unknown specie type.\n", __func__);
    }
  }
}

/**
 * @brief TBD
 */
void NSAT_init_zns(s_zns *pzns, int id_bu, double area_bu, int link_type, FILE *fpout) {

  s_id_spa *link_tmp;

  link_tmp = SPA_create_ids(id_bu, area_bu);
  pzns->link[link_type] = SPA_secured_chain_id_fwd(pzns->link[link_type], link_tmp);
}

/**
 * @brief TBD
 */
void NSAT_finalize_link_zns(s_carac_zns *pcarac_zns, int link_type, FILE *fpout) {

  s_zns *pzns;
  double area_zns;
  int nb_zns, i;
  s_id_spa *link;

  nb_zns = pcarac_zns->nb_zns;

  for (i = 0; i < nb_zns; i++) {
    pzns = pcarac_zns->p_zns[i];
    area_zns = pzns->area;
    if (pzns->link[link_type] != NULL) {
      link = SPA_divide_ids(pzns->link[link_type], area_zns);
      pzns->link[link_type] = SPA_free_ids(pzns->link[link_type], fpout);
      pzns->link[link_type] = link;
    } else {
      // LP_warning(fpout,"ZNS cell id %d is not connected with a surface cell no recharge can be calculated \n",pzns->id[NS_INTERN]);
      pzns->link[link_type] = SPA_create_id_void(); // BL to avoid segmentation fault if the zns cell is not connected with surface !!! Extremement moche bouhhhhhhh !!!
    }
  }
}

/**
 * @brief Prints summary of NSAT compartment characteristics in the log file
 */
void NSAT_print_zns(s_carac_zns *pcarac_zns, FILE *fpout) {

  s_zns *pzns;
  double area_zns;
  int nb_zns, i;
  s_id_spa *link;

  nb_zns = pcarac_zns->nb_zns;

  for (i = 0; i < nb_zns; i++) {
    pzns = pcarac_zns->p_zns[i];
    area_zns = pzns->area;
    LP_printf(fpout, "ZNS ID %d GIS %d INTERN \n", pzns->id[NS_GIS], pzns->id[NS_INTERN]);
    LP_printf(fpout, "ZNS Area : %f \n", pzns->area);
    NSAT_print_nash(pzns->pnash, fpout);
    LP_printf(fpout, "linked with SURF elements : \n");
    SPA_print_ids(pzns->link[SURF_NS], fpout);
    LP_printf(fpout, "linked with SOUT elements : \n");
    SPA_print_ids(pzns->link[AQ_NS], fpout);
  }
}

/**
 * @brief Utilitarian function to detect if HEAT species is present among transport species
 * @return YES if heat type specie found.
 */
int NSAT_is_heat_transport(int nb_species, int *spetypes) {
  int p, ans = NO;

  for (p = 0; p < nb_species; p++) {
    if (spetypes[p] == HEAT_NS) {
      ans = YES;
      break;
    }
  }
  return ans;
}