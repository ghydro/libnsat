/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: manage_carac_nsat.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_carac_nsat.c
 * @brief       Functions dealing with attributes of the s_carac_zns structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#if defined(COUPLED)
#include "NSAT.h"
#else
#include "NSAT_full.h"
#endif
#ifdef OMP
#include "omp.h"
#endif

/**
 * @brief Allocates and initializes the s_carac_zns structure
 * @return Newly allocated and set pointer
 */
s_carac_zns *NSAT_create_carac_zns() {

  s_carac_zns *pcarac;
  pcarac = new_carac_zns();
  bzero((char *)pcarac, sizeof(s_carac_zns));
  pcarac->dynzns_io = NO;
  pcarac->dyn_refresh_dt = DEFAULT_DYN_DT_NSAT;
  pcarac->nres_method = DYNSAT_SOIL_NS;

#ifdef OMP
  pcarac->psmp = new_smp();
  pcarac->psmp->nthreads = 1;
#endif

  return pcarac;
}

/**
 * @brief Parent initialization function. Builds all Nash reservoir cascades
 * for all ZNS cells.
 */
void NSAT_create_all_cascade(s_carac_zns *pcarac_zns, FILE *fpout) {

  int nb_zns, i;
  s_zns **p_zns;

#ifdef OMP
  s_smp *psmp;
  psmp = pcarac_zns->psmp;
#endif

  nb_zns = pcarac_zns->nb_zns;
  p_zns = pcarac_zns->p_zns;

#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fpout, pcarac_zns->nb_zns, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(p_zns, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_zns; i++) {
      NSAT_create_cascade(p_zns[i], fpout);
    }

#ifdef OMP
  } /* end of parallel section */
#endif
}

/**
 * @brief General function launching ZNS input water discharge calculations
 * (link with libfp water balance).
 */
void NSAT_define_all_input(s_carac_zns *pcarac_zns, s_carac_fp *pcarac_fp, int transport, FILE *fpout) {

  int nb_zns, i;
  s_zns **p_zns;

#ifdef OMP
  s_smp *psmp;
  psmp = pcarac_zns->psmp;
#endif

  nb_zns = pcarac_zns->nb_zns;
  p_zns = pcarac_zns->p_zns;

#ifdef OMP
  int taille;
  int nthreads;
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fpout, pcarac_zns->nb_zns, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(p_zns, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_zns; i++) {
      NSAT_define_input(p_zns[i], pcarac_fp, fpout);
    }

#ifdef OMP
  } /* end of parallel section */
#endif
}

/**
 * @brief General function launching ZNS hydro calculations.
 */
void NSAT_calculate_all_zns_coupled(s_carac_zns *pcarac_zns, double dt, s_carac_fp *pcarac_fp, FILE *fpout, int dynflag, double t) {

  int nb_zns, i;
  s_zns **p_zns;

#ifdef OMP
  s_smp *psmp;
  psmp = pcarac_zns->psmp;
#endif

  nb_zns = pcarac_zns->nb_zns;
  p_zns = pcarac_zns->p_zns;

#ifdef OMP
  int taille;
  int nthreads;

  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);

  psmp->chunk = PC_set_chunk_size_silent(fpout, pcarac_zns->nb_zns, nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(p_zns, nthreads, taille) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_zns; i++) {
      NSAT_define_input(p_zns[i], pcarac_fp, fpout);

      // NG : 24/01/2020 : Accounting for catchment input from surface if connected
      if (p_zns[i]->pcatchment_chasm != NULL && p_zns[i]->pcatchment_chasm->catchment_type == FP_CATCH_CHASM)
        NSAT_refresh_input(p_zns[i], fpout, dt, t);

      NSAT_calculate_zns(p_zns[i], dt, dynflag, fpout);
    }
#ifdef OMP
  } /* end of parallel section */
#endif
}

/**
 * @brief General function launching ZNS transport calculations.
 * Parrallelized over the number of NSAT cells, sequential over the number of species.
 * @internal NG : 04/04/2023 : Update of all sub-functions to deal with both SOLUTE and HEAT
 */
void NSAT_calculate_all_zns_transport(int nb_species, int *spe_types, s_carac_zns *pcarac_zns, double dt, s_carac_fp *pcarac_fp, FILE *fpout, int flag_dynNsat, double t) {

  int i, p, nb_zns = pcarac_zns->nb_zns;
  s_zns **p_zns = pcarac_zns->p_zns;

#ifdef OMP
  s_smp *psmp;
  psmp = pcarac_zns->psmp;
  int taille;
  int nthreads;

  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(fpout, nb_zns, nthreads);
  taille = psmp->chunk;

#pragma omp parallel shared(p_zns, nthreads, taille, nb_species, nb_zns) private(i)
  {
#pragma omp for schedule(dynamic, taille)
#endif

    for (i = 0; i < nb_zns; i++) {

      // Calculation of water inputs from surface/libfp (mm)
      NSAT_define_input(p_zns[i], pcarac_fp, fpout);

      // Update of water inputs if chasm-type catchments connexions are detected
      if (p_zns[i]->pcatchment_chasm != NULL && p_zns[i]->pcatchment_chasm->catchment_type == FP_CATCH_CHASM) {
        NSAT_refresh_input(p_zns[i], fpout, dt, t);
      }

      // Calculation of transport inputs from surface/libfp (ie. matter/energy fluxes (in g/m2 ou W/m2) and associated transport variable (concentration (g/m3) or temperature (K))
      NSAT_define_transport_input(nb_species, spe_types, p_zns[i], pcarac_fp, dt, fpout);

      // Update of transport inputs if chasm-type catchments connexions are detected
      // WARNING : Case of additional transport input from river infiltration not treated so far (FP_CATCH_CHASM connexion only)
      if (p_zns[i]->pcatchment_chasm != NULL && p_zns[i]->pcatchment_chasm->catchment_type == FP_CATCH_CHASM) {
        NSAT_refresh_input_transport(nb_species, spe_types, p_zns[i], fpout, dt, t);
      }

      // Flow calculation using layered reservoirs for all species
      NSAT_calculate_zns_transport(nb_species, spe_types, p_zns[i], flag_dynNsat, dt, fpout);
    }

#ifdef OMP
  } /* end of parallel section */
#endif
}

/**
 * @brief Initializes layers piles for all reservoirs of all ZNS cells
 */
void NSAT_initialize_layer_all_reservoirs(s_carac_zns *pcarac_zns, int nb_species, FILE *fpout) {

  int i, j, p, nb_zns = pcarac_zns->nb_zns;
  s_zns *pzns;
  s_reservoir_rsv *prsv;

  for (i = 0; i < nb_zns; i++) {
    pzns = pcarac_zns->p_zns[i];

    pzns->Fr = (double *)calloc(nb_species, sizeof(double));
    pzns->trvar_init = (double *)calloc(nb_species, sizeof(double));

    for (p = 0; p < nb_species; p++)
      pzns->trvar_init[p] = pzns->pnash->carac_ZNS[NS_CINI];

    if (pzns->prsv != NULL) {
      prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, pzns->prsv, BEGINNING_SPA);
      while (prsv != NULL) {
        RSV_initialize_layers_transport(prsv, nb_species, pzns->trvar_init, fpout);
        prsv = prsv->next;
      }
    }
  }
}