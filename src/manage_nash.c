/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: manage_nash.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_nash.c
 * @brief       Functions dealing with attributes of the s_nash structure
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#if defined(COUPLED)
#include "NSAT.h"
#else
#include "NSAT_full.h"
#endif

/**
 * @brief Links two Nash parameter sets
 */
s_nash *NSAT_secured_chain_nash_fwd(s_nash *pnash1, s_nash *pnash2) {
  if (pnash2 != NULL) {
    pnash1 = NSAT_chain_nash_fwd(pnash1, pnash2);
  } else {
    pnash1 = pnash2;
  }
  return pnash1;
}

/**
 * @brief Links two Nash parameter sets
 */
s_nash *NSAT_chain_nash_fwd(s_nash *pnash1, s_nash *pnash2) {
  pnash1->next = pnash2;
  pnash2->prev = pnash1;
  return (pnash1);
}

/**
 * @brief Stores all Nash parameter sets into a single array.
 * @return Allocated and filled array of s_nash* pointers
 */
s_nash **NSAT_tab_nash(s_nash *pnash, int nnash) {
  int i;
  s_nash **p_nash;

  p_nash = (s_nash **)malloc(nnash * sizeof(s_nash *));
  bzero((char *)p_nash, nnash * sizeof(s_nash *));
  pnash = (s_nash *)SPA_browse_all_struct(NASH_SPA, pnash, BEGINNING_SPA);
  for (i = 0; i < nnash; i++) {
    pnash->id[NS_INTERN] = i;
    p_nash[i] = pnash;
    pnash = pnash->next;
  }
  return p_nash;
}

/**
 * @brief Allocates and initializes a Nash parameter set
 * @return Allocated and initialized Nash structure
 */
s_nash *NSAT_create_nash(int id) {
  s_nash *pnash;

  pnash = new_nash();
  bzero((char *)pnash, sizeof(s_nash));
  pnash->id[NS_GIS] = id;
  pnash->carac_ZNS = ((double *)malloc(NS_NNASH * sizeof(double)));
  bzero((char *)pnash->carac_ZNS, NS_NNASH * sizeof(double));

  return (pnash);
}

/**
 * @brief Fills the input nsah parameters into a Nash structure
 * @internal NG : 06/12/2023 All inputs are set in meters in the user file. Code deals with mm.
 * 24/11/2024 : New `trvar_ini` parameter added to include initial transport variable (C,T) setup.
 */
void NSAT_fill_nash(s_nash *pnash, int nb_rsv, double height, double cqout, double bound_water, double r_ini, double trvar_ini) {
  pnash->nb_reservoir = nb_rsv;
  pnash->carac_ZNS[NS_HEIGHT] = height * 1000;
  pnash->carac_ZNS[NS_C_QOUT] = cqout;
  pnash->carac_ZNS[NS_BOND_WATER] = bound_water * 1000;
  pnash->carac_ZNS[NS_RINI] = r_ini * 1000;
  pnash->carac_ZNS[NS_CINI] = trvar_ini;
}

/**
 * @brief Prints Nash parameters for all ZNS cells.
 */
void NSAT_print_all_nash(s_carac_zns *pcarac_zns, FILE *fpout) {
  int i, nb_nash;
  s_nash *pnash;

  nb_nash = pcarac_zns->nb_nash;

  for (i = 0; i < nb_nash; i++) {
    pnash = pcarac_zns->p_nash[i];
    NSAT_print_nash(pnash, fpout);
  }
}

/**
 * @brief Prints Nash parameters for a single ZNS cell.
 */
void NSAT_print_nash(s_nash *pnash, FILE *fpout) {

  int j;
  LP_printf(fpout, "ID_NASH : GIS_ID %d INT_ID %d\n", pnash->id[NS_GIS], pnash->id[NS_INTERN]);
  LP_printf(fpout, "Number of reservoirs : %d \n", pnash->nb_reservoir);
  LP_printf(fpout, "Parameters : ");
  for (j = 0; j < NS_NNASH; j++) {
    LP_printf(fpout, "%s : %f ", NSAT_name_param(j), pnash->carac_ZNS[j]);
  }
  LP_printf(fpout, "\n");
}