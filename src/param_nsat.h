/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: param_nsat.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        param_nsat.h
 * @brief       NSAT library parameters and enumerations
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

// String substition shortcuts
#define YES YES_TS
#define NO NO_TS
#define EPS_NSAT EPS_TS
#define CODE_NSAT CODE_TS
#define BEGINNING_NSAT BEGINNING_SPA
#define END_NSAT END_SPA
#define SMALL_NSAT SMALL_TS

/**
 * @enum NS_enash
 * @brief Nash parameters
 */
enum NS_enash { NS_BOND_WATER, NS_C_QOUT, NS_HEIGHT, NS_RINI, NS_CINI, NS_NNASH };

/**
 * @enum id_NSAT
 * @brief NSAT cell IDs
 */
enum id_NSAT { NS_INTERN, NS_GIS, NS_NDID };

/**
 * @enum NS_nb_link
 * @brief NSAT link types
 */
enum NS_nb_link { SURF_NS, AQ_NS, NB_LINK_NS };

/**
 * @enum NS_species_types
 * @brief Transport species types
 */
enum NS_species_types { HEAT_NS, SOLUTE_NS, NS_SPE_TYPES };

/**
 * @enum NS_tstep_update
 * @brief Time indexes to fetch the number of reservoirs in the Nash cascade
 * @internal NG : 27/06/2023 : nres is now managed using an int* so we can store changes in number of reservoirs.
 * If dynamic behavior is OFF, only CURRTS_NS is used.
 */
enum NS_tstep_update { PREVTS_NS, CURRTS_NS, NS_TSTEP };

/**
 * @enum NS_reservoir_update_method
 * @brief Methods for dynamic NSAT reservoir updates :
 * `DYNSAT_SOIL_NS` : Update of nres based on soil_elevation - hydraulic head
 * `DYNSAT_HH_NS`   : Update of nres based on variation of hydraulic head only.
 */
enum NS_reservoir_update_method { DYNSAT_SOIL_NS, DYNSAT_HH_NS, NS_RES_METHOD };

/**
 * @def NB_MB_VAL_NSAT
 * @brief Number of record field in MB output file (for water flows)
 */
#define NB_MB_VAL_NSAT 4

/**
 * @def NB_MB_VAL_TRANSPORT_NSAT
 * @brief Number of transport outputs for each specie. Total number of outputs for NSAT_MB_TRANPORT files is actually,
 * for p species = NB_MB_VAL_NSAT + p*NB_MB_VAL_TRANSPORT_NSAT
 */
#define NB_MB_VAL_TRANSPORT_NSAT 6

/**
 * @def DEFAULT_DYN_DT_NSAT
 * @brief Default duration (in sec) for NSAT thickness refresh (= 30 days), if dynamic behavior is on.
 */
#define DEFAULT_DYN_DT_NSAT 2592000.0

/**
 * @def VERSION_NSAT
 * @brief NSAT library version ID
 */
#define VERSION_NSAT 0.17

/**
 * @def RHO_WATER_NS
 * @brief Water density in kg/m3
 */
#define RHO_WATER_NS 1000.0

/**
 * @def CP_WATER_NS
 * @brief Water specific heat capacity in J/kg/K
 */
#define CP_WATER_NS 4186.0