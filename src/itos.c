/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        itos.c
 * @brief       Interger-to-string conversion functions
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#if defined(COUPLED)
#include "NSAT.h"
#else
#include "NSAT_full.h"
#endif

/**
 * @brief Integer-to-string conversion function for Nash parameters
 * @return Nash parameter name
 */
char *NSAT_name_param(int iname) {
  char *name;

  switch (iname) {
  case NS_BOND_WATER: {
    name = strdup("BOND_WATER");
    break;
  }
  case NS_HEIGHT: {
    name = strdup("HEIGHT");
    break;
  }
  case NS_C_QOUT: {
    name = strdup("CQOUT");
    break;
  }
  case NS_RINI: {
    name = strdup("INITIAL_STATE");
    break;
  }
  case NS_CINI: {
    name = strdup("INITIAL_CONCENTRATION");
    break;
  }
  default: {
    name = strdup("Unknown Nash parameter");
    break;
  }
  }
  return name;
}

/**
 * @brief Integer-to-string conversion function for transport specie type
 * @return Specie type name
 */
char *NSAT_species_type(int itype) {
  char *name;

  switch (itype) {
  case SOLUTE_NS: {
    name = strdup("SOLUTE");
    break;
  }
  case HEAT_NS: {
    name = strdup("HEAT");
    break;
  }
  }
  return name;
}

/**
 * @brief Integer-to-string conversion function for dynamic NSAT refresh method
 * @return Refresh method name
 */
char *NSAT_nres_update_method(int itype) {
  char *name;

  switch (itype) {
  case DYNSAT_SOIL_NS: {
    name = strdup("SOIL-HYDRAULIC_HEAD");
    break;
  }
  case DYNSAT_HH_NS: {
    name = strdup("HYDRAULIC_HEAD_ONLY");
    break;
  }
  }
  return name;
}