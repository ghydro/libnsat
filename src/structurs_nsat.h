/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: structurs_nsat.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        structurs_nsat.c
 * @brief       NSAT library structures
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

typedef struct nash s_nash;
typedef struct zone_non_sat s_zns;
typedef struct carac_nsat s_carac_zns;

/**
 * @struct nash
 * @brief Nash reservoir parameters set structure
 */
struct nash {
  /*!< Nash dataset ID */
  int id[NS_NDID];
  /*!< Number of reservoirs of the NASH cascade as set in the input file */
  int nb_reservoir;
  /*!< Reservoir settings (bond water, emptying constant, res height). Sized over NS_NNASH */
  double *carac_ZNS;
  /*!< Forward chain pointer */
  s_nash *next;
  /*!< Backward chain pointer */
  s_nash *prev;
};

/**
 * @struct zone_non_sat
 * @brief Unsaturated zone column/cell structure
 */
struct zone_non_sat {
  /*!< ZNS cell ID */
  int id[NS_NDID];
  /*!< Pointer towards the corresponding reservoir chain */
  s_reservoir_rsv *prsv;
  /*!< Number of reservoirs at current and previous time step. Allocated over NS_TSTEP */
  int *nres;
  /*!< Link between surface balance elements (SURF_NS) and aquifer elements (AQ_NS) */
  s_id_spa *link[NB_LINK_NS];
  /*!< Pointer toward the set of parameters of the ZNS cell */
  s_nash *pnash;
  /*!< Mean soil elevation on top of the ZNS cell */
  double soil_elevation;
  /*!< Mean hydraulic heads below the ZNS cell at the previous and current NSAT update. Allocated over NS_TSTEP */
  double *hydhead;
  /*!< Area (m2) of the ZNS cell */
  double area;
  /*!< Water volume to be transfered to the aquifer cell below the ZNS cell */
  double Vr;
  /*!< Pointer toward the RSV budget structure */
  s_wat_bal_rsv *wat_bal;
  /*!< Forward chain pointer */
  s_zns *next;
  /*!< Backward chain pointer */
  s_zns *prev;
  /*!< Pointer to a surface catchment in case of infiltration via a chasm */
  s_catchment_fp *pcatchment_chasm;
  /*!< NSAT cell activation status of dynamic behavior. Set to dynzns_io by default */
  int dyn_io;
  /*!< Transport fluxes to be transfered to the aquifer cell below the ZNS cell. Sized over nb_species*/
  double *Fr;
  /*!< Transport variable to initialize reservoir layers.In dynamic behavior mode, used to fetch
  aquifer transport variable at the bottom of the ZNS cell. Sized over nb_species */
  double *trvar_init;
};

/**
 * @struct carac_nsat
 * @brief Library master structure. Gives access to general settings
 * and all calculation objects.
 */
struct carac_nsat {
  /*!< Number of different nash in the simulation */
  int nb_nash;
  /*!< Number of ZNS calculation unit */
  int nb_zns;
  /*!< Tabular of pointer to the NASH parameters */
  s_nash **p_nash;
  /*!< Tabular of pointer to the ZNS calculation unit */
  s_zns **p_zns;
  /*!< General activation status of dynamic NSAT behavior. Set to NO by default */
  int dynzns_io;
  /*!< Dynamic NSAT refresh time step to update thickness of the NSAT compartment */
  double dyn_refresh_dt;
  /*!< Dynamic NSAT method for update of reservoir chain. Set to DYNSAT_SOIL_NS by default */
  int nres_method;

#ifdef OMP
  s_smp *psmp;
#endif
};

#define new_nash() ((s_nash *)malloc(sizeof(s_nash)))
#define new_zns() ((s_zns *)malloc(sizeof(s_zns)))
#define new_carac_zns() ((s_carac_zns *)malloc(sizeof(s_carac_zns)))