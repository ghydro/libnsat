/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: manage_output.c
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "reservoir.h"
#include "spa.h"
#if defined(COUPLED)
#include "NSAT.h"
#else
#include "NSAT_full.h"
#endif
#ifdef OMP
#include "omp.h"
#endif

/**
 * @file        manage_output.c
 * @brief       Functions dealing with libnsat IO management (init file, water/transport balance calculations, etc.)
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

/**
 * @brief Prints NSAT (HYDRO mode) formatted header in mass balance output file.
 */
void NSAT_print_header(FILE *fpout, FILE *fp) {

  if (fpout != NULL) {
    fprintf(fpout, "%s ", "DAY ID_GIS ID_INTERN FROM_SURF(m3/s) TO_SOUT(m3/s) STOCKED_IN_ZNS(m3/s) ERROR(m3/s)");
    fprintf(fpout, "\n");
  } else {
    LP_error(fp, "In libnsat%4.2f : in %s, function %s : File struct is NULL.\n", VERSION_NSAT, __FILE__, __func__);
  }
}

/**
 * @brief Sets NSAT attributes from a init_from_file restart file.
 * @warning NG : Not sure this feature is functional and consistent with more recent
 * versions of libnsat. Has not been modified for ages.
 */
void NSAT_init_from_file(FILE *fpin, s_carac_zns *pcarac_zns, FILE *fplog) {

  int i, j, errno;
  int nb_zns, rsv_length;
  int position, rsv_length_file;
  s_zns *zns;
  s_id_io *id_out;
  s_reservoir_rsv *prsv;
  double z_file;

  nb_zns = pcarac_zns->nb_zns;

  for (i = 0; i < nb_zns; i++) {
    zns = pcarac_zns->p_zns[i];
    rsv_length = zns->nres[CURRTS_NS];

    if (zns->prsv != NULL) {
      prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, zns->prsv, BEGINNING_SPA);

      fread(&rsv_length_file, sizeof(int), 1, fpin);
      if (rsv_length != rsv_length_file) {
        LP_error(fplog, "While reading FinState_NSAT.bin for zns GIS %d INTERN %d max number of rsv in file %d max number of rvs in structur", zns->id[NS_GIS], zns->id[NS_INTERN], rsv_length_file, rsv_length);
      }

      for (j = 0; j < rsv_length_file; j++) {

        fread(&z_file, sizeof(double), 1, fpin);
        if (prsv != NULL) {
          prsv->z = z_file;
        } else {
          LP_error(fplog, "While reading FinState_NSAT.bin for zns GIS %d INTERN %d max rsv in pzns %d max rsv in file %d", zns->id[NS_GIS], zns->id[NS_INTERN], j, rsv_length_file);
        }
        prsv = prsv->next;
      }
      fread(&rsv_length_file, sizeof(int), 1, fpin);
    }
  }
}

/**
 * @brief Parent function. Launches the calculation of the hydro NSAT mass balance.
 */
void NSAT_write_mb_zns(double t, double dt, double t_day, s_out_io *pout, s_carac_zns *pcarac_zns, FILE *fp) {

  double t_out = pout->t_out[CUR_IO];

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      NSAT_print_mb_formatted(pcarac_zns, t_day, pout, dt, fp);
    } else {
      NSAT_print_mb_bin(pcarac_zns, pout, dt, fp);
    }
  }
}

/**
 * @brief Computes and prints the HYDRO NSAT mass balance in FORMATTED format.
 * @internal Not sure the mask selection feature is functional.
 */
void NSAT_print_mb_formatted(s_carac_zns *pcarac_zns, double t_out, s_out_io *pout, double dt, FILE *fp) {
  int i, j, nb_zns;
  s_zns *zns;
  s_id_io *id_out;
  double bilan;

  nb_zns = pcarac_zns->nb_zns;

  if (pout->type_out == ALL_IO) {

    for (i = 0; i < nb_zns; i++) {

      zns = pcarac_zns->p_zns[i];
      bilan = zns->wat_bal->q[RSV_INFILT] - zns->wat_bal->q[RSV_SOUT] - zns->wat_bal->q[RSV_BIL];
      fprintf(pout->fout, "%f %d %d ", t_out, zns->id[NS_GIS], zns->id[NS_INTERN]);
      fprintf(pout->fout, "%15.7e %15.7e %15.7e %15.7e", zns->wat_bal->q[RSV_INFILT], zns->wat_bal->q[RSV_SOUT], zns->wat_bal->q[RSV_BIL], bilan);
      fprintf(pout->fout, "\n");
    }
  } else {
    id_out = pout->id_output;
    while (id_out != NULL) {
      i = id_out->id;
      zns = pcarac_zns->p_zns[i];
      bilan = zns->wat_bal->q[RSV_INFILT] - zns->wat_bal->q[RSV_SOUT] - zns->wat_bal->q[RSV_BIL];
      fprintf(pout->fout, "%f %d %d ", t_out, zns->id[NS_GIS], zns->id[NS_INTERN]);
      fprintf(pout->fout, "%15.7e %15.7e %15.7e %15.7e", zns->wat_bal->q[RSV_INFILT], zns->wat_bal->q[RSV_SOUT], zns->wat_bal->q[RSV_BIL], bilan);
      fprintf(pout->fout, "\n");
      id_out = id_out->next;
    }
  }
}

/**
 * @brief Saves the final reservoir states in a binary file.
 */
void NSAT_print_final_state(s_carac_zns *pcarac_zns, s_out_io *pout, FILE *fp) {

  int i, j, nb_zns, rsv_length;
  s_zns *zns;
  s_id_io *id_out;
  s_reservoir_rsv *prsv;
  double **val;

  nb_zns = pcarac_zns->nb_zns;

  for (i = 0; i < nb_zns; i++) {

    zns = pcarac_zns->p_zns[i];
    rsv_length = zns->nres[CURRTS_NS];

    if (zns->prsv != NULL) {
      val = (double **)malloc(sizeof(double *));
      val[0] = (double *)malloc(rsv_length * sizeof(double));
      prsv = (s_reservoir_rsv *)SPA_browse_all_struct(RSV_SPA, zns->prsv, BEGINNING_SPA);

      for (j = 0; j < rsv_length; j++) {
        val[0][j] = prsv->z;
        prsv = prsv->next;
      }
      IO_print_bloc_bin(val, rsv_length, 1, pout->fout);
    }
  }
}

/**
 * @brief Parent function. Launches the calculation and print of the NSAT
 * mass balance (Hydro mode) in binary format.
 */
void NSAT_print_mb_bin(s_carac_zns *pcarac_zns, s_out_io *pout, double dt, FILE *fp) {

  int nele_tot, type_out, output;
  double **val;
  s_id_io *id_out;
  FILE *fpout;

  output = NB_MB_VAL_NSAT;
  fpout = pout->fout;
  type_out = pout->type_out;

  if (type_out == ALL_IO) {
    nele_tot = pcarac_zns->nb_zns;
  } else {
    nele_tot = IO_length_id(pout->id_output);
  }

  val = NSAT_get_mb_val_bin(pout, pcarac_zns, nele_tot, type_out, dt, fp);
  IO_print_bloc_bin(val, nele_tot, output, fpout);
}

/**
 * @brief Fills the double** with mass balance flows to be written in binary format (HYDRO mode)
 * @return Filled mass balance double** for the current time step.
 * @internal Dummy mask selection feature as **val matrix is always sized over nele_tot !
 */
double **NSAT_get_mb_val_bin(s_out_io *pout, s_carac_zns *pcarac_zns, int nele_tot, int type_out, double dt, FILE *fp) {
  int i, j, k;
  int output, output_i;
  int nb_zns;
  s_zns *zns;
  s_id_io *id_out;
  double bilan;
  double **val;
#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_zns->psmp;
#endif

  output = NB_MB_VAL_NSAT;
  val = (double **)malloc(output * sizeof(double *));
  for (i = 0; i < output; i++) {
    val[i] = (double *)malloc(nele_tot * sizeof(double));
  }

  if (type_out == ALL_IO) {
    nb_zns = pcarac_zns->nb_zns;
#ifdef OMP

    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);
    psmp->chunk = PC_set_chunk_size_silent(fp, pcarac_zns->nb_zns, nthreads);
    taille = psmp->chunk;

#pragma omp parallel shared(nb_zns, pcarac_zns, val, nthreads, taille) private(i, output_i, bilan, zns)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (i = 0; i < nb_zns; i++) {
        output_i = 0;
        zns = pcarac_zns->p_zns[i];

        bilan = zns->wat_bal->q[RSV_INFILT] - zns->wat_bal->q[RSV_SOUT] - zns->wat_bal->q[RSV_BIL];
        val[output_i][i] = zns->wat_bal->q[RSV_INFILT];
        output_i++;

        val[output_i][i] = zns->wat_bal->q[RSV_SOUT];
        output_i++;

        val[output_i][i] = zns->wat_bal->q[RSV_BIL];
        output_i++;
        val[output_i][i] = bilan;
        output_i++;
      }
#ifdef OMP
    } /* end of parallel section */
#endif
  } else {
    id_out = pout->id_output;
    i = 0;
    while (id_out != NULL) {
      k = id_out->id;
      zns = pcarac_zns->p_zns[k];

      bilan = zns->wat_bal->q[RSV_INFILT] - zns->wat_bal->q[RSV_SOUT] - zns->wat_bal->q[RSV_BIL];
      val[output_i][i] = zns->wat_bal->q[RSV_INFILT];
      output_i++;
      val[output_i][i] = zns->wat_bal->q[RSV_SOUT];
      output_i++;
      val[output_i][i] = zns->wat_bal->q[RSV_BIL];
      output_i++;
      val[output_i][i] = bilan;
      output_i++;
      i++;
      id_out = id_out->next;
    }
  }
  return val;
}

/**
 * @brief Prints the correspondance file for NSAT cells IDs
 */
void NSAT_print_corresp(s_carac_zns *pcarac_zns, int mod, FILE *fp) {
  int nb_zns, i;
  FILE *fpout;
  char *name_mod, *name;
  s_zns *zns;
  name_mod = IO_name_mod(mod);

  name = (char *)malloc((strlen(getenv("RESULT")) + strlen(name_mod) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/%s_corresp_file.txt", getenv("RESULT"), name_mod);
  fpout = fopen(name, "w");
  free(name_mod);

  if (fpout == NULL) {
    LP_error(fp, "In libnsat%4.2f : in file %s, function %s : Cannot open file %s", VERSION_NSAT, __FILE__, __func__, name);
  }

  fprintf(fpout, "ID_INTERN ID_GIS\n");
  nb_zns = pcarac_zns->nb_zns;
  for (i = 0; i < nb_zns; i++) {
    zns = pcarac_zns->p_zns[i];
    fprintf(fpout, "%d %d\n", zns->id[NS_INTERN], zns->id[NS_GIS]);
  }
  fclose(fpout);
  free(name);
}

/**
 * @brief Prints a summary of NSAT module characteristics in the log file.
 */
void NSAT_print_abstract(s_carac_zns *pchar_nsat, FILE *flog) {

  int i, nb_nash, nb_zns;
  double Surf_zns = 0;
  nb_nash = pchar_nsat->nb_nash;
  nb_zns = pchar_nsat->nb_zns;

  for (i = 0; i < nb_zns; i++) {
    Surf_zns += pchar_nsat->p_zns[i]->area;
  }

  LP_printf(flog, "\t --> Number of NSAT calculation units : %d \n", nb_zns);
  LP_printf(flog, "Surface : %f m2\n", Surf_zns);
  LP_printf(flog, "\t --> Number of Nash structures : %d\n", nb_nash);
  LP_printf(flog, "Parameters : \n");
  NSAT_print_NASH_param(pchar_nsat, flog);
}

/**
 * @brief Prints a table of the entire set of Nash parameters in the log file
 */
void NSAT_print_NASH_param(s_carac_zns *pchar_nsat, FILE *flog) {

  s_nash *pnash;
  int nb_nash, i, j;
  char *name;
  nb_nash = pchar_nsat->nb_nash;

  for (j = 0; j < NS_NNASH; j++) {
    name = NSAT_name_param(j);
    LP_printf(flog, "%s ", name);
    free(name);
  }
  LP_printf(flog, "\n");
  for (i = 0; i < nb_nash; i++) {
    pnash = pchar_nsat->p_nash[i];
    for (j = 0; j < NS_NNASH; j++) {
      LP_printf(flog, "%f ", pnash->carac_ZNS[j]);
    }
    LP_printf(flog, "\n");
  }
}

/**
 * @brief General function launching NSAT_TRANSPORT output files
 */
void NSAT_write_mb_zns_transport(int nb_species, double t, double dt, double t_day, s_out_io *pout, s_carac_zns *pcarac_zns, FILE *flog) {
  double t_out = pout->t_out[CUR_IO];

  if (t_out <= t && t_out > t - dt) {
    if (pout->format == FORMATTED_IO) {
      NSAT_print_transport_mb_formatted(nb_species, pcarac_zns, t_day, pout, dt, flog);
    } else {
      NSAT_print_transport_mb_bin(nb_species, pcarac_zns, pout, dt, flog);
    }
  }
}

/**
 * @brief Prints the transport header for output file in formatted format according to the number of species
 */
void NSAT_print_transport_header(int nb_species, int *spe_types, FILE *fpout) {
  int p;
  fprintf(fpout, "%s ", "DAY ID_GIS ID_INTERN ");

  for (p = 0; p < nb_species; p++) {
    fprintf(fpout, "%s ", "WATER_FROM_SURF_m3/s WATER_TO_SOUT_m3/s STOCK_WATER_m3/s ERROR_WATER_m3/s ");
    if (spe_types[p] == SOLUTE_NS) {
      fprintf(fpout, "%s ", "MFLUX_FROM_SURF_g/m2 MFLUX_TO_SOUT_g/m2 CONC_FROM_SURF_g/m3 CONC_TO_SOUT_g/m3 STOCK_FLUX_g/m2 ERROR_FLUX_g/m2");
    } else { // HEAT case
      fprintf(fpout, "%s ", "MFLUX_FROM_SURF_W/m2 MFLUX_TO_SOUT_W/m2 T_FROM_SURF_°C T_TO_SOUT_°C STOCK_FLUX_W/m2 ERROR_FLUX_W/m2");
    }
  }
  fprintf(fpout, "\n");
}

/**
 * @brief Computes and prints mass balances for all zns columns at a given time step in formatted format
 */
void NSAT_print_transport_mb_formatted(int nb_species, s_carac_zns *pcarac_zns, double t_day, s_out_io *pout, double dt, FILE *fp) {
  int i, j;
  int nb_zns = pcarac_zns->nb_zns;
  s_zns *pzns;
  s_id_io *id_out;

  if (pout->type_out == ALL_IO) {
    for (i = 0; i < nb_zns; i++) {
      pzns = pcarac_zns->p_zns[i];
      NSAT_print_budget_transport(nb_species, pzns, t_day, pout, fp);
    }
  } else {
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);

    while (id_out != NULL) {
      i = id_out->id;
      pzns = pcarac_zns->p_zns[i];
      NSAT_print_budget_transport(nb_species, pzns, t_day, pout, fp);
      id_out = id_out->next;
    }
  }
}

/**
 * @brief Prints and computes transport mass balance for a ZNS column at a given time-step
 */
void NSAT_print_budget_transport(int nb_species, s_zns *pzns, double t_day, s_out_io *pout, FILE *fp) {
  double *mb_values;
  int j;
  int nb_rec = NB_MB_VAL_NSAT + nb_species * NB_MB_VAL_TRANSPORT_NSAT;

  fprintf(pout->fout, "%f %d %d ", t_day, pzns->id[NS_GIS], pzns->id[NS_INTERN]);

  mb_values = NSAT_budget_transport(nb_species, pzns, fp);
  for (j = 0; j < nb_rec; j++)
    fprintf(pout->fout, "%15.7e ", mb_values[j]);
  fprintf(pout->fout, "\n");
  free(mb_values);
}

/**
 * @brief Computes full budget at a given time step for a single ZNS column
 * @return Table of doubles containing mass balance values
 */
double *NSAT_budget_transport(int nb_species, s_zns *pzns, FILE *fp) {
  int nb_rec = NB_MB_VAL_NSAT + nb_species * NB_MB_VAL_TRANSPORT_NSAT;
  double *mb_values;
  int p, pos = 0;
  double error_water = 0.;
  double error_flux = 0.;

  mb_values = (double *)malloc(nb_rec * sizeof(double));
  if (mb_values == NULL)
    LP_error(fp, "In libnsat%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation.\n", VERSION_NSAT, __FILE__, __func__, __LINE__);
  bzero((char *)mb_values, nb_rec * sizeof(double));

  // Water MB
  error_water = 0.;
  mb_values[pos] = pzns->wat_bal->q[RSV_INFILT]; // m3/s
  error_water += mb_values[pos];
  pos++;

  mb_values[pos] = pzns->wat_bal->q[RSV_SOUT]; // m3/s
  error_water -= mb_values[pos];
  pos++;

  mb_values[pos] = pzns->wat_bal->q[RSV_BIL]; // m3/s   either > 0 or < 0
  error_water -= mb_values[pos];
  pos++;

  mb_values[pos] = error_water;
  pos++;

  // Transport MB
  for (p = 0; p < nb_species; p++) {
    error_flux = 0.;
    mb_values[pos] = pzns->wat_bal->trflux[p][RSV_INFILT]; // g/m2
    error_flux += mb_values[pos];
    pos++;

    mb_values[pos] = pzns->wat_bal->trflux[p][RSV_SOUT]; // g/m2
    error_flux -= mb_values[pos];
    pos++;

    mb_values[pos] = pzns->wat_bal->trvar[p][RSV_INFILT];
    pos++; // g/m3
    mb_values[pos] = pzns->wat_bal->trvar[p][RSV_SOUT];
    pos++; // g/m3

    mb_values[pos] = pzns->wat_bal->trflux[p][RSV_BIL]; // g/m2    either > 0 or < 0
    error_flux -= mb_values[pos];
    pos++;
    mb_values[pos] = error_flux;
    pos++; // Flux MB error
  }
  return mb_values;
}

/**
 * @brief Launches mass balances calculations for transport in binary format
 */
void NSAT_print_transport_mb_bin(int nb_species, s_carac_zns *pcarac_zns, s_out_io *pout, double dt, FILE *flog) {

  double **values;
  int neletot, type_out = pout->type_out;
  int nb_rec = NB_MB_VAL_NSAT + nb_species * NB_MB_VAL_TRANSPORT_NSAT;
  s_id_io *id_out;

  if (type_out == ALL_IO) {
    neletot = pcarac_zns->nb_zns;
  } else {
    neletot = IO_length_id(pout->id_output);
  }
  values = NSAT_get_mb_val_bin_transport(nb_species, pout, pcarac_zns, neletot, type_out, dt, flog);
  IO_print_bloc_bin(values, neletot, nb_rec, pout->fout);
}

/**
 * @brief Builds and fills up the matrix of outputs for all species at each time step
 * @return Matrix of doubles containing all budget values for all ZNS columns
 */
double **NSAT_get_mb_val_bin_transport(int nb_species, s_out_io *pout, s_carac_zns *pcarac_zns, int neletot, int type_out, double dt, FILE *flog) {
  double **values;
  double *mb_values;
  int nb_zns, i, j, p;
  int nb_rec = NB_MB_VAL_NSAT + nb_species * NB_MB_VAL_TRANSPORT_NSAT;
  s_id_io *id_out;
  s_zns *pzns;

#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_zns->psmp;
#endif

  values = (double **)malloc(nb_rec * sizeof(double *));
  if (values == NULL)
    LP_error(flog, "In libnsat%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", VERSION_NSAT, __FILE__, __func__, __LINE__);
  for (i = 0; i < nb_rec; i++) {
    values[i] = (double *)malloc(neletot * sizeof(double));
    if (values[i] == NULL)
      LP_error(flog, "In libnsat%4.2f : Error in file %s, in function %s at line %d : Bad memory allocation while writing binary outputs.\n", VERSION_NSAT, __FILE__, __func__, __LINE__);
  }

  if (type_out == ALL_IO) {

#ifdef OMP
    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);
    psmp->chunk = PC_set_chunk_size_silent(flog, neletot, nthreads);
    taille = psmp->chunk;
#pragma omp parallel shared(neletot, pcarac_zns, values, nthreads, taille) private(i, j, pzns, mb_values)
    {
#pragma omp for schedule(dynamic, taille)
#endif

      for (i = 0; i < neletot; i++) {
        pzns = pcarac_zns->p_zns[i];
        mb_values = NSAT_budget_transport(nb_species, pzns, flog);
        for (j = 0; j < nb_rec; j++)
          values[j][i] = mb_values[j];
        free(mb_values);
      }
    }
#ifdef OMP
  } // Fin de section parrallélisée
#endif
  else {
    id_out = IO_browse_id(pout->id_output, BEGINNING_IO);
    while (id_out != NULL) {
      i = id_out->id;
      pzns = pcarac_zns->p_zns[i];
      mb_values = NSAT_budget_transport(nb_species, pzns, flog);
      for (j = 0; j < nb_rec; j++)
        values[j][i] = mb_values[j];
      free(mb_values);
      id_out = id_out->next;
    }
  }
  return values;
}