/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libnsat
 * FILE NAME: functions_nsat.h
 *
 * CONTRIBUTORS: Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Unsaturated zone simulated using
 * a Nash cascade of reservoirs.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libnsat Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        functions_nsat.h
 * @brief       NSAT library functions
 * @author      Nicolas GALLOIS, Baptiste LABARTHE, Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

// in manage_zns.c
s_zns *NSAT_create_zns(int, s_nash *);
s_zns *NSAT_secured_chain_zns_fwd(s_zns *, s_zns *);
s_zns *NSAT_chain_zns_fwd(s_zns *, s_zns *);
void NSAT_calculate_zns(s_zns *, double, int, FILE *);
void NSAT_dynamic_chain_update(s_zns *, FILE *); // NG : 28/06/2023 : RSV chain structure update in case of dynamic thickness activated
void NSAT_create_cascade(s_zns *, FILE *);
s_zns *NSAT_check_zns(s_zns *, s_zns *, int, FILE *);
s_zns **NSAT_tab_zns(s_zns *, int, FILE *);
void NSAT_define_input(s_zns *, s_carac_fp *, FILE *);
void NSAT_init_zns(s_zns *, int, double, int, FILE *);
void NSAT_finalize_link_zns(s_carac_zns *, int, FILE *);
void NSAT_print_zns(s_carac_zns *, FILE *);
void NSAT_check_chasm_connexion(s_carac_zns *, FILE *);
void NSAT_refresh_input(s_zns *, FILE *, double, double);
void NSAT_define_transport_input(int, int *, s_zns *, s_carac_fp *, double, FILE *);
void NSAT_refresh_input_transport(int, int *, s_zns *, FILE *, double, double);
int NSAT_is_heat_transport(int, int *);
void NSAT_dynamic_chain_update_transport(s_zns *, int, FILE *);

// in manage_nash.c
s_nash *NSAT_secured_chain_nash_fwd(s_nash *, s_nash *);
s_nash *NSAT_chain_nash_fwd(s_nash *, s_nash *);
s_nash **NSAT_tab_nash(s_nash *, int);
s_nash *NSAT_create_nash(int);
void NSAT_fill_nash(s_nash *, int, double, double, double, double, double);
void NSAT_print_all_nash(s_carac_zns *, FILE *);
void NSAT_print_nash(s_nash *, FILE *);

// in manage_carac_nsat.c
s_carac_zns *NSAT_create_carac_zns();
void NSAT_create_all_cascade(s_carac_zns *, FILE *);
void NSAT_define_all_input(s_carac_zns *, s_carac_fp *, int, FILE *);
void NSAT_calculate_all_zns_coupled(s_carac_zns *, double, s_carac_fp *, FILE *, int, double);
void NSAT_calculate_all_zns_transport(int, int *, s_carac_zns *, double, s_carac_fp *, FILE *, int, double);
void NSAT_initialize_layer_all_reservoirs(s_carac_zns *, int, FILE *);

// in itos.c
char *NSAT_name_param(int);
char *NSAT_species_type(int);
char *NSAT_nres_update_method(int);

// in manage_output.c
void NSAT_init_from_file(FILE *, s_carac_zns *, FILE *);
void NSAT_print_header(FILE *, FILE *);
void NSAT_write_mb_zns(double, double, double, s_out_io *, s_carac_zns *, FILE *);
void NSAT_print_mb_formatted(s_carac_zns *, double, s_out_io *, double, FILE *);
void NSAT_print_mb_bin(s_carac_zns *, s_out_io *, double, FILE *);
double **NSAT_get_mb_val_bin(s_out_io *, s_carac_zns *, int, int, double, FILE *);
void NSAT_print_corresp(s_carac_zns *, int, FILE *);
void NSAT_print_abstract(s_carac_zns *, FILE *);
void NSAT_print_NASH_param(s_carac_zns *, FILE *);
void NSAT_print_final_state(s_carac_zns *, s_out_io *, FILE *);
void NSAT_print_transport_header(int, int *, FILE *);
void NSAT_write_mb_zns_transport(int, double, double, double, s_out_io *, s_carac_zns *, FILE *);
void NSAT_print_transport_mb_formatted(int, s_carac_zns *, double, s_out_io *, double, FILE *);
void NSAT_print_budget_transport(int, s_zns *, double, s_out_io *, FILE *);
double *NSAT_budget_transport(int, s_zns *, FILE *);
void NSAT_print_transport_mb_bin(int, s_carac_zns *, s_out_io *, double, FILE *);
double **NSAT_get_mb_val_bin_transport(int, s_out_io *, s_carac_zns *, int, int, double, FILE *);
void NSAT_calculate_zns_transport(int, int *, s_zns *, int, double, FILE *);